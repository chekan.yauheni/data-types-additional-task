package com.epam.rd.autotasks.simplecalculations;

import java.util.Scanner;

public class SimpleCalculations {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter value for x: ");
        int x = input.nextInt();
        System.out.print("Enter value for y: ");
        int y = input.nextInt();
        System.out.println(
                "x + y = " + (x + y) + "\n" +
                "x - y = " + (x - y) + "\n" +
                "x * y = " + (x * y) + "\n" +
                "x / y = " + (x / y)
        );
    }
}
