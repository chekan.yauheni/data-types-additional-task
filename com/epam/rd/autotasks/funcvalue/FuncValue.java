package com.epam.rd.autotasks.funcvalue;

import java.util.Scanner;

public class FuncValue {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Find the value of the function 'z = ((a - 3) * b / 2) + c'");
        System.out.print("Enter value for a: ");
        int a = input.nextInt();
        System.out.print("Enter value for b: ");
        double b = input.nextInt();
        System.out.print("Enter value for c: ");
        int c = input.nextInt();
        double z = ((a - 3) * b / 2) + c;
        System.out.println("((a - 3) * b / 2) + c = " + z);
    }
}
