package com.epam.rd.autotasks.righttriangle;

import java.util.Scanner;

public class RightTriangle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Calculate the area and the perimeter of the right triangle:");
        System.out.print("Enter the value for side A: ");
        double sideA = input.nextDouble();
        System.out.print("Enter the value for side B: ");
        double sideB = input.nextDouble();
        double area = (sideA / 2) * sideB;
        double perimeter = sideA + sideB + Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
        System.out.printf("Area = %s\nPerimeter = %s", area, perimeter);
    }
}
